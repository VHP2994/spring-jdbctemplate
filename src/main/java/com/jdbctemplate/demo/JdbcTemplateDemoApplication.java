package com.jdbctemplate.demo;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.jdbctemplate.demo.dao.EmployeeDao;
import com.jdbctemplate.demo.model.Employee;

@SpringBootApplication
public class JdbcTemplateDemoApplication {

	public static void main(String[] args) {
		ApplicationContext atx=new ClassPathXmlApplicationContext("beans.xml");
	        EmployeeDao empDao = (EmployeeDao) atx.getBean("employeeDAO");
	        Employee emp = new Employee();
	        emp.setName("Madhu");
	        emp.setSalary(60000);
	        emp.setDept("HR");
	        empDao.insertEmployee(emp);
	}

}
