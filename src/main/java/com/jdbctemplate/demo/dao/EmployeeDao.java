package com.jdbctemplate.demo.dao;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;

import com.jdbctemplate.demo.model.Employee;

public class EmployeeDao {

	private DataSource dataSource;
	private JdbcTemplate jdbcTemplate;

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}
	
	
	public void insertEmployee(Employee emp) {
		String query="insert into employee (name,salary,dept) values (?,?,?)";
		jdbcTemplate =new JdbcTemplate(dataSource);
		Object[] input=new Object [] {emp.getName(),emp.getSalary(),emp.getDept()};
		jdbcTemplate.update(query,input);
		
	}
	
}
